FROM alpine:latest
MAINTAINER Marty Oehme <marty.oehme@gmail.com>

RUN apk add --no-cache jq curl
